#include<stdio.h>
#include<png.h>
#include<zlib.h>
#include<math.h>
#include<stdlib.h>

#include"display.h"

// TODO add COLOR to PNG

#if 1
#define MONOCHROME
#endif


// sets color to PNG pixel
static void set_rgb(png_byte *ptr, uint8_t val) {
#ifndef MONOCHROME
	// set black for points in the set
	if (val == 0) {
		ptr[0] = val;	// Red
		ptr[1] = val;	// Green
		ptr[2] = val;	// Blue
		return;
	}

	// Blue-Yellow color scheme
	if (val < 102) {
		ptr[0] = 0;	// Red
		ptr[1] = 0;	// Green
		ptr[2] = -1.1 * (val - 0.6 * 255);	// Blue
		return;
	}
	if (val < 153) {
		ptr[0] = 1.65 * (val - 0.5 * 255);	// Red
		ptr[1] = 1.65 * (val - 0.5 * 255);	// Green
		ptr[2] = -1.1 * (val - 0.6 * 255);	// Blue
		return;
	}
	ptr[0] = 1.65 * (val - 0.5 * 255);	// Red
	ptr[1] = 1.65 * (val - 0.5 * 255);	// Green
	ptr[2] = 0;	// Blue
#else
	ptr[0] = val;	// Red
	ptr[1] = val;	// Green
	ptr[2] = val;	// Blue
#endif
	return;
}

// Create PNG image
int32_t create_png(uint8_t *buffer) {
	png_structp png_ptr;
	png_infop info_ptr;

	FILE *fp = fopen("img/mandelbrot.png", "wb");
	if (!fp) {
		return -1;
	}

	// Create png pointer
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if (png_ptr == NULL) {
		fclose(fp);
		return -2;
	}

	// Info pointer
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL) {
		fclose(fp);
		png_destroy_write_struct(&png_ptr, NULL);
		return -3;
	}

	// in case of error
	if (setjmp(png_jmpbuf(png_ptr))) {
		fclose(fp);
		png_destroy_write_struct(&png_ptr, &info_ptr);
		return -4;
	}

	png_init_io(png_ptr, fp);

	png_set_IHDR(png_ptr, info_ptr, X_PIXELS, Y_PIXELS, 8, PNG_COLOR_TYPE_RGB,
			PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);


	// Set title
	png_text title_text;
	title_text.compression = PNG_TEXT_COMPRESSION_NONE;
	title_text.key = "Title";
	title_text.text = "mandelbrot.png";
	png_set_text(png_ptr, info_ptr, &title_text, 1);

	// Write image info
	png_write_info(png_ptr, info_ptr);

	// Allocate memory for one row (3 bytes per pixel - RGB)
	png_bytep row = (png_bytep) malloc(3 * X_PIXELS * sizeof(png_byte));

	// Write image data
	int x, y;
	for (y = 0 ; y < Y_PIXELS ; y++) {
		for (x = 0 ; x < X_PIXELS ; x++) {
			set_rgb(&(row[x * 3]), buffer[y * X_PIXELS + x]);
		}
		png_write_row(png_ptr, row);
	}

	// End write
	png_write_end(png_ptr, NULL);

	if (fp != NULL) fclose(fp);
	if (info_ptr != NULL) png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
	if (png_ptr != NULL) png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
	if (row != NULL) free(row);

	return 0;
}

// Linear interpolation
static double lerp(double val1, double val2, double t) {
	return (1 - t) * val1 + t * val2;
}

// Converts Mandelbrot iterations into color, using interpolation and such
// making color appear as a continuous gradient. Avoids stripes.
// Input:
// 	iter_arr	- pointer to an array of iterations it took to exit `mandelbrot_point`
//
// Output:
// 	byter		- pointer to 
// stores output on heap, return pointer
Bytes *conv(IterArray *iter_arr) {
	Bytes *bytes = malloc(sizeof(Bytes));

	// (re)calculate increment
	// TODO inefficient, pass coordinates instead of recalculating but w/e
	const f128 X_DEL = (X1 - X0)/ X_PIXELS;
	const f128 Y_DEL = (Y0 - Y1)/ Y_PIXELS;

	uint32_t x, y;

	for (y = 0 ; y < Y_PIXELS ; y++) {
		const f128 Y_POINT = Y0 - y*Y_DEL;
		for (x = 0 ; x < X_PIXELS ; x++) {
			const f128 X_POINT = X0 + x*X_DEL;
			double iters = 0.0 + iter_arr->line[y].point[x];
	
			if (iters >= MAX_ITER) bytes->bytes[y*X_PIXELS + x] = 0;
			else {
				const double LOG_ZN = log(X_POINT*X_POINT + Y_POINT*Y_POINT + 1) / 2;
				const double NU = log(LOG_ZN / log(1<<8))/log(1<<8);
				iters = iters + 1.0 - NU;
				const uint8_t COLOR1 = 255 - 255*floor(iters);
				const uint8_t COLOR2 = 255 - 255*(floor(iters)+1);
				bytes->bytes[y*X_PIXELS + x] = lerp(COLOR1, COLOR2, remainder(iters, 1));
			}
		}
	}
	return bytes;
}

// poor man's display
// Prints to stdout terminal
void term_display(IterArray *iter_arr) {
	uint32_t x_counter = 0;
	uint32_t y_counter = 0;

	for( ;y_counter < Y_PIXELS_TERM; y_counter++) {
		for( ;x_counter < X_PIXELS_TERM; x_counter++) {
			// Assign "color" to a pixel
			if (iter_arr->line[y_counter].point[x_counter] >= MAX_ITER) {
				printf("●");
			} else if (iter_arr->line[y_counter].point[x_counter] > MAX_ITER/500.0) {
				printf("•");
			} else if (iter_arr->line[y_counter].point[x_counter] > MAX_ITER/900.0) {
				printf("·");
			} else {
				printf(" ");
			}
		}
		printf("\n");	// print new row
		x_counter = 0;
	}
}

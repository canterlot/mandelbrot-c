#include<stdint.h>
#include "mandelbrot.h"

void term_display(IterArray *iter_arr);
int32_t create_png(uint8_t *buffer);
Bytes *conv(IterArray *iter_arr);

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <png.h>
#include <zlib.h>
#include <unistd.h>
#include <sys/wait.h>

#include"mandelbrot.h"


// Calculate whether point is in or out of Mandelbrot set.
//
// Point is said to be inside the set if the output is equal to `MAX_ITER`
// (maximum iterations per point)
static uint32_t mandelbrot_point(const Point point) {

	// check if point lies inside main Cardioid.
	// If it does, automatically output `MAX_ITER` to save computation time.
	const f128 Q_VALUE = (point.x - .25) * (point.x - .25) + point.y * point.y;
	if (((point.y * point.y) * 0.25) >= (Q_VALUE * (Q_VALUE + (point.x - 0.25)))) {
		return MAX_ITER;
	}
	// Check if point lies inside 2nd period bulb
	if ((point.x + 1) * (point.x + 1) + point.y * point.y <= 1.0/16.0) {
		return MAX_ITER;
	}

	f128 x = 0;
	f128 y = 0;
	f128 xtemp;

	uint32_t period = 0;
	uint32_t count = 0;

	f128 xold = 0;
	f128 yold = 0;

	for (; count < MAX_ITER; count++) {
		// Equivalent to z^2 + c ; where z, c are complex.
		xtemp = x*x - y*y + point.x;
		y = 2*x*y + point.y;
		x = xtemp;

		// If magnitude of result is larger than 2, we automatically know
		// that point does not lie inside Mandelbrot set.
		//
		// Amount of iterations (count) will be used for coloring.
		if (x*x + y*y >= 4) {
			break;
		}

		// If result is stuck in a period (like -1 0 -1 0 -1....),
		// then point is inside the set
		if (x == xold && y == yold) {
			return MAX_ITER;
		}
		period++;
		if (period > 20) {
			period = 0;
			xold = x;
			yold = y;
		}
	}
	return count;
}


// Calculate one pixel tall line of Mandelbrot set for easier job distribution
// For multiprocessing
// 
// 	line		- A line of X coordinates which lie on same Y coordinates
// 	x_pixels	- Amount of X coordinates to process and output
//	iter_line	- pointer to a row in array of iterations
static void mandelbrot_line(
		const Array1 *line,
		const uint32_t x_pixels,
		IterLine *iter_line)
{
	uint32_t x_count;
	for (x_count = 0; x_count < x_pixels; x_count++) {
		iter_line->point[x_count] = mandelbrot_point( (Point){ line->x_arr[x_count], line->y } );
	}
}

// Precompute a line of x-coordinates, they will not be changed when
// calculating Mandelbrot set
static Array1 *get_coordinate_line(
		const Box boundary,
		const uint32_t x_pixels)
{
	Array1 *line = malloc(sizeof(Array1));	

	// calculate x increment
	const f128 X_DEL = (boundary.point2.x - boundary.point1.x) / x_pixels;
	f128 x_coord = boundary.point1.x;	// initial value

	// create a line of x coordinates, equally spaced
	uint32_t x_count;
	for (x_count = 0; x_count < x_pixels; x_count++) {
		line->x_arr[x_count] = x_coord;
		x_coord += X_DEL;
	}
	// set to something so it is not floating
	line->y = 0;
	return line;
}

// Calculate a 2d array of iterations inside boundary x_pixels by y_pixels large
// Returns array of iterations x_pixels by y_pixels large
//
// Non-multi-processing
IterArray *mandelbrot(
		const Box boundary,
		const uint32_t x_pixels,
		const uint32_t y_pixels)
{
	IterArray *iter_arr = malloc(sizeof(IterArray));

	// allocate memory to x coordinates line
	Array1 *line = get_coordinate_line(boundary, x_pixels);

	// calculate y increment
	const f128 Y_DEL = (boundary.point1.y - boundary.point2.y) / y_pixels;

	uint32_t y_count;
	f128 y_coord = boundary.point1.y;	// initial value

	for ( y_count = 0; y_count < y_pixels; y_count++) {
		line->y = y_coord;
		mandelbrot_line(line, x_pixels, &iter_arr->line[y_count]);

		progress_bar((f128) y_count/Y_PIXELS);
		y_coord -= Y_DEL;	// increment
	}
	free(line);
	return iter_arr;
}



struct BufLine {
	int32_t p_number;
	IterLine line;
};

static int32_t mandelbrot_pipe_error_handler(
		const ssize_t res,
		const int32_t p_number,
		const uint32_t y_count,
		const uint32_t y_pixels)
{
	if (res < 0) {
		printf("ERROR: could not read from pipe\n");
		return -4;
	}
	if (res != sizeof(struct BufLine)) {
		printf("ERROR: read wrong amount of bytes pipe (%ld/%lu B)\n",
				res,
				sizeof(struct BufLine));
		printf("ERROR: most likely due to saturated I/O\n");
		printf("ERROR: try lowering image resolution or cores used\n");
		return -4;
	}
	if ((p_number > CORES_TO_USE) || (p_number < 0)) {
		printf("ERROR: value read from pipe is corrupt; value is `%d' but must be between `1' - `%d'\n",
				p_number,
				CORES_TO_USE);
		printf("ERROR: most likely due to saturated I/O\n");
		printf("ERROR: try lowering image resolution or cores used\n");
		return -4;
	}

	// Check that no illicit memory access will take place
	if (((y_count + p_number) >= y_pixels) || ((y_count + p_number) < 0)) {
		printf("ERROR: out of bounds memory; trying to access element `%d' in array of length `%d'\n",
				y_count + p_number,
				y_pixels);
		printf("ERROR: most likely due to saturated I/O\n");
		printf("ERROR: try lowering image resolution or cores used\n");
		return -3;
	}
	return 0;
}

// Calculate a 2d array of iterations inside boundary x_pixels by y_pixels large
// Returns array of iterations x_pixels by y_pixels large
//
// multi-processing
//
// Error codes:
// 	PIPE_CREATION_ERR					-1
// 	FORK_CREATION_ERR					-2
// 	ILLICIT_MEMORY_ACCESS_ITER_ARRAY	-3
// 	READ_PIPE_ERR						-4
Res mandelbrot_mp(
		const Box boundary,
		const uint32_t x_pixels,
		const uint32_t y_pixels)
{
	// Main heap memory
	IterArray *iter_arr = malloc(sizeof(IterArray));

	// allocate memory to x coordinates line
	Array1 *line = get_coordinate_line(boundary, x_pixels);

	// allocate mem for array of buffer lines IterLine
	IterLine *buffer_line = malloc(sizeof(IterLine));

	// calculate y increment
	const f128 Y_DEL = - (boundary.point1.y - boundary.point2.y) / y_pixels;

	int32_t p_number = 0;	// process number
	int32_t p_count;			// process counter
	pid_t pid = -1;	// PID

	uint32_t y_count;	// y pixel counter
	f128 y_coord = boundary.point1.y;	// initial value

	// Main loop
	for ( y_count = 0; y_count < y_pixels; y_count += CORES_TO_USE)
	{
		// create data pipe
		int data_fd[2];
		if ((pipe(data_fd) == -1)) {	// if pipe fails, exit
			free(line);	free(iter_arr);	free(buffer_line);
			return (Res) {-1, NULL};
		}

		// Create children
		for (p_count = 0; (p_count < CORES_TO_USE) && ((y_count + p_count + 1) < y_pixels); p_count++) {
			pid = fork();
			if (pid == -1) {	// return error in case process cannot be created
				free(line);	free(iter_arr);	free(buffer_line);	// free before exiting
				return (Res) {-2, NULL};
			}
			if (pid == 0) { // if child
				p_number = p_count + 1;	// gives each process a unique p_number
				break;	// do not let children create processes
			}	
		}

		// Parent job
		if (pid > 0)
		{ 
			close(data_fd[1]); // close output file desc.

			//y_coord += Y_DEL*p_number;
			line->y = y_coord;	// change y value to calculate next line

			// calculate line
			mandelbrot_line(line, x_pixels, &iter_arr->line[y_count]);

			for (p_count = 0; (p_count < CORES_TO_USE) && ((y_count + p_count + 1) < y_pixels); p_count++) {
				wait(NULL);	// wait for child to finish

				// read IterLine calculated by child from pipe into buffer
				// pipe functions as a FIFO (first in first out)
				struct BufLine pipe_buf;
				ssize_t res = read(data_fd[0], &pipe_buf, sizeof(struct BufLine));
				{
					// handles common pipe errors
					int32_t err = mandelbrot_pipe_error_handler(
												res,
												pipe_buf.p_number,
												y_count,
												y_pixels);
					if (err < 0) {
						free(line);	free(iter_arr);	free(buffer_line);	// free before exiting
						return (Res) {err, NULL};
					}
				}
				// move data to IterArray from buffer
				iter_arr->line[y_count + (uint32_t)pipe_buf.p_number] = pipe_buf.line;
			}
			close(data_fd[0]);	// close input file desc.
		}
		// Child job
		else 
		{ 
			//if (y_count > Y_PIXELS - 20) { printf("\ny=%d; p=%d\n", y_count, p_number); fflush(stdout);}
			close(data_fd[0]); // close input file desc.

			y_coord += Y_DEL*p_number;	// change y value 
			line->y = y_coord;

			// calculate line
			mandelbrot_line(line, x_pixels, buffer_line);

			// write via pipe
			write(data_fd[1], &(struct BufLine) {p_number, *buffer_line}, sizeof(struct BufLine));
			close(data_fd[1]); // close output file desc.
			exit(EXIT_SUCCESS);	// exit child process
		}
		y_coord += Y_DEL * CORES_TO_USE;	// increment Y coord.
#ifndef NDEBUG
		progress_bar((f128) y_count/Y_PIXELS);
#endif
	}
	progress_bar(1.0);
	free(line);	// drop from heap to avoid mem-leaks
	free(buffer_line);
	printf("\n");	// new line to not write over progress bar
	return (Res) {0, iter_arr};
}

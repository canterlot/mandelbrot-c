// ************************************************************************ //
// Title:   Mandelbrot In C
// Author:  Vitalijs Kudrins
// Version:	0.03
// Description: Mandelbrot plot generator in C.
//
// TODO:
//  * Add argument passing via command line
//      * pass image size
//      * max iterations
//      * output type
//      * multi processing cores
//      * add documentation via print_help()
//
//  * Add multi-processing via fork() and pipe()
//  	* refactor mandelbrot() to a more multi processing			DONE
//  	  friendly structure
//  	* create a pipe between processes
//  	* create job-creation
//
//  * Add automatic zoom sequence generation
// ************************************************************************* //

#include<stdlib.h>
#include<stdio.h>
#if 0
// TODO
#include<argp.h>
#endif

#include "display.h"

#if 0
#define RUN_TERM_DISPLAY
#endif

enum output_type {
	PNG,
	TERM
};

static int run_mandelbrot(int output_type) {
	printf("Calculating between point [%lf, %lf] and point [%lf, %lf] (x, y)\n", X0, Y0, X1, Y1);
	printf("Maximum %i iterations per point\n", MAX_ITER);
	printf("Image of size %ix%i px\n", X_PIXELS, Y_PIXELS);

	switch (output_type) {
		case PNG:
		{ // If creating PNG
			Res res = mandelbrot_mp(
					(Box){
						(Point){X0, Y0},
						(Point){X1, Y1}
					},
					X_PIXELS,
					Y_PIXELS);
			if (res.status != 0) {
				printf("Mandelbrot calculations failed\n");
				return -1;
			}
			printf("Calculations complete!\n");
			printf("Creating PNG image...\n");

			// Convert iterations to PNG bytes
			Bytes *bytes = conv((IterArray*)res.data);

			// free an IterArray malloc'ed in mandelbrot_mp
			free((IterArray*)res.data);

			// create and save PNG
			printf("create_png exit status: %i\n", create_png(bytes->bytes));

			// free Bytes_t malloc'ed in conv()
			free(bytes);
			break;
		}
		case TERM:
		{ // If running with terminal display
			IterArray *iter_arr = mandelbrot(
					(Box) {
						(Point){X0, Y0},
						(Point){X1, Y1}
					},
					X_PIXELS_TERM,
					Y_PIXELS_TERM);
			term_display(iter_arr);
			free(iter_arr);
			break;
		}
		default:
		{
			printf("Error\n");
			return -1;
		}
	}
	puts("Done!");
	return 0;
}


// ARGS (= default_value):
// 	center_point_x =	-0.75,
// 	center_point_y =	0,
// 	width =				1,
// 	aspect_ratio = 		2/3,
// 	max_iterations = 	10000,
//	
// TODO add command line argument support
int main(int argc, char *argv[]) {

	enum output_type o_type = PNG;
	run_mandelbrot(o_type);

	return 0;
}

// Help message
// TODO
static void print_help() {
	printf("* Mandelbrot graph generator in C *\n");
	printf("** License: GPLv3\n");
	printf("\n");
	printf("Usage:\n");
	printf("\tmandelc \n");
}


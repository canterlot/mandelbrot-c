#include<stdio.h>
#include<sys/ioctl.h>
#include<unistd.h>
#include<math.h>
#include<string.h>

#include "mandelbrot.h"


// Prints a progress bar line
static void print_progress_bar(const uint8_t total_spaces, const uint8_t filled_spaces) {

	uint8_t count = 0;
	printf("\r%s", PROGRESS_STR);
	{
		// Get terminal width for prettier progress bar (push to the back)
		const struct winsize w;
		ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

		// how many spaces to print
		const int32_t column_diff = w.ws_col - total_spaces - strlen(PROGRESS_STR) - 5;

		// Push bar to the end of terminal
		if (column_diff >= 0) {	// don't push if there is not enough space
			for ( ; count <= column_diff; count++) {
				printf(" ");
			}
		}
	}
	// Print the progress bar itself
	count = 0;
	printf("[");
	for ( ; count < total_spaces; count++) {
		if (count <= filled_spaces) {
			printf("#");
		} else {
			printf(".");
		}
	}
	printf("]");

	// Force output out of stdout buffer, otherwise it won't print immediately
	fflush(stdout);

	return;
}

// Wrapper for print_progress_bar
// Decides whether to print again or not, depending if bar has made any progress
void progress_bar(const f128 precentage) {

	const uint8_t total_spaces = PROGRESS_BAR_LEN;

	static uint8_t old_filled_spaces = 0;
	uint8_t filled_spaces = (uint8_t)ceil(precentage*total_spaces);
	
	// If progress bar is unchanged, do not print
	if (filled_spaces != old_filled_spaces) {
		print_progress_bar(total_spaces, filled_spaces);
		old_filled_spaces = filled_spaces;
	}
	return;
}

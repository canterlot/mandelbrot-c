#include<stdint.h>

#if 0 
#define NDEBUG
#endif

// maximum amount of iterations per pixel
#define MAX_ITER	20000U

#define CORES_TO_USE	4U

#define X	-0.745428
#define Y	0.113009
#define R	0.00003


// X-axis (real) boundary
#define X0	X-(1.25*R)
#define X1	X+(1.25*R)

// Y-axis (imaginary) boundary
#define Y0	Y+R
#define Y1	Y-R

#define X_PIXELS	3000U
#define Y_PIXELS 	2000U

// TERMINAL DISPLAY DIMENSIONS
#define X_PIXELS_TERM	200U
#define Y_PIXELS_TERM 	X_PIXELS_TERM/2

typedef long double f128;

// A point on 2D plane
typedef struct point {
	f128 x;
	f128 y;
} Point;

// Single x-line of x coordinates
// X coordinates do not need to be re-calculated, only y coordinate will be changed
typedef struct {
	f128 x_arr[X_PIXELS];
	f128 y;
} Array1;

// line of iterations along X axis
typedef struct {
	uint32_t point[X_PIXELS];
} IterLine;

// array integer iterations for each point in Mandelbrot set
typedef struct {
	IterLine line[Y_PIXELS];
} IterArray;

// A box defined by 2 coordinates
typedef struct {
	Point point1;	// x0, y0
	Point point2;	// x1, y1
} Box;

// Wrapper for output, includes status for error handling and pointer to data itself
typedef struct {
	int32_t status;
	void *data;
} Res;

// DISPLAY
// display.c
#define BYTES_PER_PIXEL	3U
typedef struct {
	uint8_t bytes[X_PIXELS * Y_PIXELS * BYTES_PER_PIXEL];
} Bytes;

// PROGRESS BAR
// prog_bar.c
#define PROGRESS_BAR_LEN	40U
#define PROGRESS_STR	"Progress: "
void progress_bar(const f128 precentage);

// mandelbrot.c
IterArray *mandelbrot(Box boundary, uint32_t x_pixels, uint32_t y_pixels);
Res mandelbrot_mp(Box boundary, uint32_t x_pixels, uint32_t y_pixels);

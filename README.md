# C Mandelbrot Generator
## Written entirely C*
### Current features:
* Naive algorithm
* Print image in terminal
* Print image as PNG (monochrome)
* Smooth color gradient
* Multiprocessing (a little wonky but works)
---
### TODO features (priority):
* Fix Makefile (very high)
* Argument passing via command line (high)
* Add usage documentation via `README` and `--help` (high)
* Utilization of perturbation theory for x100 performance boost at high zooms (high)
* Stabilize concurrency and pipes (medium)
* Different image color schemes (low)
* Automatic animation creation (very low)
* Automatic image processing such as histogram equalization or mean curvature blur (very low)
---
Yes, there are countless Mandelbrot generators.
The idea with this one is to make full use of C's speed and employ all known optimization techniques.
Perhaps in future to also build a UI.

**NOTE:** Currently only built for and tested on Linux (POSIX),
might work on Windows if Cygwin is used, but that is merely speculations.

\*Python file is quite old and is mostly redundant.
Most of the features have been implemented in C using better techniques.
Never mind the fact that C is so much fasted :-)
---
![Mandelbrot](img/mandelbrot_mean_curvature_blur.png)
---
License: GPLv3

Credits: Vitalijs Kudrins, Gustav Olofsson

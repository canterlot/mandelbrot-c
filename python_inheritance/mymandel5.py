#!/usr/bin/python3
import time
import math
import numpy as np
from PIL import Image
import multiprocessing as mp
import matplotlib.pyplot as plt

#TODO: Utilize Pertubation Theory for a significant performance boost
#       This will big redisign of code.
START_TIME = time.perf_counter()

MAX_ITER = int(1e2)
#          [x1,  y1, x2, y2]
#BOUNDARY = [-2, 1.25, 0.5, -1.25]
CENTER = [0.3750001200618555+0.05, -0.2166393884377127+1e-2]
SIZE = [1e-2, 1e-2]
BOUNDARY = [CENTER[0]-SIZE[0], CENTER[1]+SIZE[1],
            CENTER[0]+SIZE[0], CENTER[1]-SIZE[1]]

BOUNDARY = [-2, 1.25, 0.5, -1.25]
#TODO: Put all of this in main()
#TODO: Create a cli
def correct_boundry(boundary_list):
    '''Corrects impropper boundary_list'''
    if boundary_list[0] == boundary_list[2]:
        print("Error: 0 width on x-axis")
        return 0
    if boundary_list[1] == boundary_list[3]:
        print("Error: 0 width on y-axis")
        return 0
    if boundary_list[0] > boundary_list[2]:
        buff = boundary_list[0]
        boundary_list[0] = boundary_list[2]
        boundary_list[2] = buff

    if boundary_list[3] > boundary_list[1]:
        buff = boundary_list[3]
        boundary_list[3] = boundary_list[1]
        boundary_list[1] = buff
    return boundary_list
BOUNDARY = correct_boundry(BOUNDARY)

ASPECT_RATIO = (BOUNDARY[2] - BOUNDARY[0])/(BOUNDARY[1] - BOUNDARY[3])
XAXIS_POINTS = 500
YAXIS_POINTS = int(XAXIS_POINTS / ASPECT_RATIO)
STEP = abs(BOUNDARY[2] - BOUNDARY[0]) / float(XAXIS_POINTS)

LOCK = mp.Lock()
PBAR = mp.Value('i', 0)

MAX_ITER_LOG = math.log10(MAX_ITER)
#TODO: Make colors more customizable
M_RED = 0
K_RED = 0
M_GREEN = 0
K_GREEN = 0
M_BLUE = 0
K_BLUE = 0

#color limits:
RED_MAX = 255
GREEN_MAX = 255
BLUE_MAX = 70

def main():
    '''Organizes everything in on func'''
    buffer = mandelbrot_main()

    print(f"Calculation done in {round(time.perf_counter() - START_TIME, 1)} second(s).")
    print("Creating image.")

    prepare_colors(limited_max_of_array(buffer, MAX_ITER-1), np.min(buffer))
    filename = create_filename()
    data_to_image(buffer, filename)


def mandelbrot_point(x_point, y_point):
    '''Calculates how many iterations is takes for a point to reach 2,
       returns an int up to MAX_ITER'''
    #TODO: Reimplement this part in C for massive performance boost

    #is point inside main Cardioid check:
    #TODO: Add a check to skip unnecessaty calculations when
    #               out of main Cardioid and 2nd bulb bounds
    q_val = (x_point - .25)**2 + y_point**2
    if ((y_point**2)*0.25) >= (q_val*(q_val+(x_point-0.25))):
        return MAX_ITER - 1
    #is point inside 2nd period bulb check:
    if (x_point + 1)**2 + y_point**2 <= 1/16:
        return MAX_ITER - 1

    z_val = complex(0.0, 0.0)
    c_val = complex(x_point, y_point)
    period = 0
    z_old = 0

    for count in range(MAX_ITER):
        z_val = z_val * z_val + c_val
        if abs(z_val) >= 2:
            break
        if z_val == z_old:
            return MAX_ITER - 1

        #Check for periodicity, aka if z_n= 0, z_n+1= -1, z_n+2= 0... etc
        period += 1
        if period > 20:
            period = 0
            z_old = z_val
    return count

def mandelbrot_main():
    '''Saves iterations data for each point in a given BOUNDARY'''
    data = np.zeros((YAXIS_POINTS, XAXIS_POINTS))

    #offset for symmetry mirroring if there are even or odd amount of pixels in y-axis
    offset_calc = (YAXIS_POINTS + 1) % 2
    weird_mirror_flag = False   #weird mirroring is when |y1>0| < |y2<0|

    if BOUNDARY[1] * BOUNDARY[3] < 0:   #Check if symmetry around X-axis can be used
        if abs(BOUNDARY[1]) < abs(BOUNDARY[3]):
            #if |y1| < |y2|
            weird_mirror_flag = True
            switch_y_border_keep_sign()
        y_axis_points = offset_calc + math.ceil(YAXIS_POINTS*\
                                BOUNDARY[1]/abs(BOUNDARY[1]-BOUNDARY[3]))
    else:
        y_axis_points = YAXIS_POINTS

    #step_list is a list of all y-coords for which rows of our set are calculated
    step_list = [None]*y_axis_points
    step_list = [BOUNDARY[1] - STEP*i for i in range(len(step_list))]

    #TODO: Maybe increase job size to reduce process creation overhead
    with mp.Pool() as proc:
        #data = np.array(proc.starmap(mandelbrot_row, enumerate(step_list)))
        data_mp = np.array(proc.map(mandelbrot_row, step_list))

    #copy data_mp to a larger array data for mirroring purposes
    for i in range(y_axis_points):
        data[i] = data_mp[i]

    if y_axis_points != YAXIS_POINTS:   #if symmetry can be used
        y_axis_symmetric = YAXIS_POINTS - y_axis_points
        for row in range(y_axis_symmetric):     #mirror around x-axis
            data[row+y_axis_points] = data[y_axis_points-row-offset_calc-1]
            print("Progress: "+str(int(100*(row+y_axis_points)/YAXIS_POINTS))+"%   ", end='\r')
    #fix for weird mirroring
    if weird_mirror_flag:
        data = np.flip(data, 0)
        switch_y_border_keep_sign()

    print("Progress: 100%")
    return data

def mandelbrot_row(y_point):
    '''Calculates one row of pixels of mandelbrot set'''
    data_row = [None]*XAXIS_POINTS
    x_point = BOUNDARY[0]
    for column in range(XAXIS_POINTS):
        data_row[column] = mandelbrot_point(x_point, y_point)
        x_point += STEP
    #uses mutex (lock) to safely and consistently show progress
    LOCK.acquire()
    PBAR.value += 1
    print("Progress: "+ str(int(100*PBAR.value/YAXIS_POINTS))+"%  ", end='\r')
    LOCK.release()

    return data_row

def switch_y_border_keep_sign():
    '''incase of weird symmetry where border |y2<0| is greater than |y1>0|,
        run before and after mandelbrot_main'''
    signs = [BOUNDARY[1], BOUNDARY[3]]
    for index, item in enumerate(signs):
        if item < 0:
            signs[index] = -1
        else:
            signs[index] = 1
    buff = BOUNDARY[1]
    BOUNDARY[1] = abs(BOUNDARY[3])*signs[0]
    BOUNDARY[3] = abs(buff)*signs[1]

def prepare_colors(max_iter, min_iter):
    '''creates global variables for use in count_to_color() instead of
        recalculating same thing over and over'''
    global K_BLUE, K_RED, M_BLUE, M_RED, M_GREEN, K_GREEN
    min_iter_log = math.log10(min_iter+1)
    max_iter_log = math.log10(max_iter)
    iter_diff = max_iter_log-min_iter_log

    M_RED = -min_iter_log*RED_MAX/iter_diff
    K_RED = RED_MAX/iter_diff

    M_GREEN = -min_iter_log*GREEN_MAX/iter_diff
    K_GREEN = GREEN_MAX/iter_diff

    M_BLUE = BLUE_MAX*max_iter_log/iter_diff
    K_BLUE = -BLUE_MAX/iter_diff

def count_to_color(count):
    '''Converts count of iterations (int) in mandelbrot_point to a RGB
       (list [int,int,int]); blue or yellow colour'''
#TODO: Redo for continuous gradient colour... somehow...
    val = math.log10(count+1)
    if val == MAX_ITER_LOG:
        return (0, 0, 0)

    red = int(M_RED + K_RED*val)
    green = int(M_GREEN + K_GREEN*val)
    blue = int(M_BLUE + K_BLUE*val)
    #       R     G     B
    return (red, green, blue)

def limited_max_of_array(array, limit_value):
    '''return max of an array which is less than limit_value'''
    len_array = len(array)
    list_of_maxes = [None]*len_array

    def limited_max_of_row(row, limit_value):
        last_largest = 0
        for item in row:
            if item > last_largest:
                if item < limit_value:
                    last_largest = item
        return last_largest

    for index, row in enumerate(array):
        list_of_maxes[index] = limited_max_of_row(row, limit_value)
    last_largest = 0
    for item in list_of_maxes:
        if item > last_largest:
            last_largest = item
    return last_largest

def histogram_of_rgb(image):
    '''This outputs 3 normalized histograms in one array of images RGB channels respectively.'''
    img_shape = np.shape(image)
    histogram = np.zeros((img_shape[2], 256))

    for rgb in range(img_shape[2]):
        for row in range(img_shape[0]):
            for column in range(img_shape[1]):
                value = image[row, column, rgb]
                histogram[rgb][value] += 1
        histogram[rgb] /= img_shape[0] * img_shape[1]

    return histogram

def s_transform_rgb(histogram):
    '''Outputs 3 histogram equalization transforms in one array, one for each RGB in that order'''
    s_transform = np.zeros(np.shape(histogram))
    scalar = [RED_MAX/255, GREEN_MAX/255, BLUE_MAX/255]
    for rgb in range(3):
        sumtotal = 0
        for index, value in enumerate(histogram[rgb]):
            sumtotal += value * scalar[rgb]
            s_transform[rgb][index] = 255*sumtotal
    for rgb in range(3):
        s_transform[rgb][0] = 0
    return s_transform

def histogram_equalization(image):
    img_shape = np.shape(image)
    image_eql = np.uint8(np.empty(img_shape))
    s_transform = s_transform_rgb(histogram_of_rgb(image))
    x = list(range(256))
    titles = ['Red', 'Green', 'Blue']
    for i in range(3):
        plt.subplot(1, 3, i+1)
        plt.plot(x, s_transform[i], '#242424')
        plt.xlabel("Value (L)")
        plt.ylabel("S(L)")
        plt.title(f"{titles[i]}")

    plt.show()
    print('Cmulative distributive function complete.')

    for rgb in range(img_shape[2]):
        print(f'Progress: {int(100*rgb/3)}%  ', end='\r')
        for row in range(img_shape[0]):
            for column in range(img_shape[1]):
                value = image[row][column][rgb]
                #if value == 0:
                #    image_eql[row][column][rgb] = 0
                #else:
                image_eql[row][column][rgb] = int(round(s_transform[rgb][value], 0))
    print("Progress: 100%")
    return image_eql

def data_to_image(data, filename):
    '''Puts RGB values from count_to_color into an image and saves it'''
#TODO: implement image saving and altering image names for zooming animation of mandelbrot
#TODO: implement multiprocessing here (unnecessaty for small images)
#TODO: implement image compression
    data_rgb = np.uint8(np.empty((YAXIS_POINTS, XAXIS_POINTS, 3)))

    for row in range(YAXIS_POINTS):
        for column in range(XAXIS_POINTS):
            data_rgb[row][column] = np.array(count_to_color(data[row][column]))
        print("Progress: "+str(int(100*row/YAXIS_POINTS))+"%   ", end='\r')
    print("Progress: 100%")

    img = Image.fromarray(data_rgb)
    #img.show()
    img.save(filename)
    print(f"Done! Saved to {filename}")
#    print("Equalizing histogram.")
#    data_rgb_eql = histogram_equalization(data_rgb)
#    img = Image.fromarray(data_rgb_eql)
#    filename = filename[:-4]+'__hist_eql.png'
#    img.save(filename)
#    print(f"Done! Saved to {filename}")


def create_filename():
    '''Creates a mandelbrot filename following (date, border, iteration, img_size)'''
    date = time.localtime()
    date_str = str(date[0])
    for unit in date[1:6]:
        date_str += '-' + str(unit)

    border_str = f'border_x=({BOUNDARY[0]}_{BOUNDARY[2]})_y=({BOUNDARY[1]}_{BOUNDARY[3]})'
    image_size_str = f'{XAXIS_POINTS}x{YAXIS_POINTS}p'
    iter_str = f'iters={MAX_ITER}'

    save_to_path = f'images/mandelbrot__{date_str}__{border_str}__{iter_str}__{image_size_str}.png'
    return save_to_path


if BOUNDARY != 0:
    main()


#argX = float(sys.argv[1])
#argY = float(sys.argv[2])
#print(f"Calculating for x={argX} y={argY}")
#print(mandelbrot_point(argX, argY))

END_TIME = time.perf_counter()
print(f"Program run completed in {round(END_TIME - START_TIME, 1)} second(s).")
